# Expense Tracker

# Main description

This app is created using react and nestjs, the frontend is hosted in: `https://d1id9clqqakhza.cloudfront.net/` and the backend
is on: `https://expense-tracker-server-m.herokuapp.com/`

# Funcionality

Expense Tracker allows you to track your expenses sorted by month, in order to add an expense you need to first set your settings (first time entering the website) then you can add your expenses and see them on the overview page.

# Running this proyect

Client and Server has both their respective readme to help you run them.

# Authentication

Authentication is handled by Auth0.
