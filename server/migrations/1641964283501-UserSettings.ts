import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserSettings1641964283501 implements MigrationInterface {
  name = 'UserSettings1641964283501';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "user_settings" ("id" serial PRIMARY KEY, "email" character varying NOT NULL, "budget" integer NOT NULL, "current_month" integer NOT NULL DEFAULT 1)`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "user_settings"`);
  }
}
