import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserExpense1641964303660 implements MigrationInterface {
  name = 'UserExpense1641964303660';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "user_expense" ("id" serial PRIMARY KEY, "email" character varying NOT NULL, "category" character varying NOT NULL, "amount" integer NOT NULL, "name" character varying NOT NULL, "month" integer NOT NULL)`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE user_expense`);
  }
}
