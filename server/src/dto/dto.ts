export class CreateExpenseDto {
  name: string;
  email: string;
  amount: number;
  category: string;
  month: number;
}

export class UpsertUserSettingsDto {
  email: string;
  budget: number;
  current_month: number;
}
