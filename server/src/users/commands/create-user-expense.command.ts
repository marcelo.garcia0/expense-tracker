export class CreateExpenseCommand {
  constructor(
    public readonly name: string,
    public readonly email: string,
    public readonly amount: number,
    public readonly category: string,
    public readonly month: number,
  ) {
    this.name = name;
    this.email = email;
    this.amount = amount;
    this.category = category;
    this.month = month;
  }
}
