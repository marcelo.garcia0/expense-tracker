export class UpsertUserSettingsCommand {
  constructor(
    public readonly email: string,
    public readonly budget: number,
    public readonly current_month: number,
  ) {
    this.email = email;
    this.budget = budget;
    this.current_month = current_month;
  }
}
