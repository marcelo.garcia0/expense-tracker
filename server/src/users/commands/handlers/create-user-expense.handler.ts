import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateExpenseCommand } from '../create-user-expense.command';
import { UserExpense } from '../../entities/user-expense.entity';

@CommandHandler(CreateExpenseCommand)
export class CreateExpenseHandler
  implements ICommandHandler<CreateExpenseCommand>
{
  constructor(
    @InjectRepository(UserExpense)
    private userExpenseRepository: Repository<UserExpense>,
  ) {}

  async execute(command: CreateExpenseCommand) {
    const { name, email, amount, category, month } = command;
    const expense = new UserExpense();
    expense.name = name;
    expense.email = email;
    expense.amount = amount;
    expense.category = category;
    expense.month = month;
    return await this.userExpenseRepository.insert(expense);
  }
}
