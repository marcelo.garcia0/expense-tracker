import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpsertUserSettingsCommand } from '../upsert-user-settings.command';
import { UserSettings } from '../../entities/user-settings.entity';

@CommandHandler(UpsertUserSettingsCommand)
export class UpsertUserSettingsHandler
  implements ICommandHandler<UpsertUserSettingsCommand>
{
  constructor(
    @InjectRepository(UserSettings)
    private userSettingsRepository: Repository<UserSettings>,
  ) {}

  async execute(command: UpsertUserSettingsCommand) {
    const { email, budget, current_month } = command;
    const userSettings = await this.userSettingsRepository.findOne({ email });
    const settings = userSettings ? userSettings : new UserSettings();
    settings.email = email;
    settings.budget = budget;
    settings.current_month = current_month;
    return await this.userSettingsRepository.save(settings);
  }
}
