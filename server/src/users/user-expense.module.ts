import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserExpense } from './entities/user-expense.entity';
import { UsersController } from './user-expense.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { CreateExpenseHandler } from './commands/handlers/create-user-expense.handler';
import { GetUsersExpenseHandler } from './queries/handlers/get-users-expense.handler';
import { UserSettings } from './entities/user-settings.entity';
import { UpsertUserSettingsHandler } from './commands/handlers/upsert-user-settings.handler';
import { GetUserSettingsHandler } from './queries/handlers/get-user-settings.handler';

@Module({
  imports: [TypeOrmModule.forFeature([UserExpense, UserSettings]), CqrsModule],
  controllers: [UsersController],
  providers: [
    CreateExpenseHandler,
    GetUsersExpenseHandler,
    UpsertUserSettingsHandler,
    GetUserSettingsHandler,
  ],
})
export class UsersExpenseModule {}
