import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateExpenseDto, UpsertUserSettingsDto } from 'src/dto/dto';
import { CreateExpenseCommand } from './commands/create-user-expense.command';
import { UpsertUserSettingsCommand } from './commands/upsert-user-settings.command';
import { GetUserSettingsQuery } from './queries/get-user-settings.query';
import { GetUsersExpenseQuery } from './queries/get-users-expense.query';

@Controller('user-expense')
export class UsersController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get(':email')
  async getUserExpenses(@Param() payload: { email: string }) {
    console.log('reaches here');
    return await this.queryBus.execute(new GetUsersExpenseQuery(payload.email));
  }

  @Get('settings/:email')
  async getUserSettings(@Param() payload: { email: string }) {
    return await this.queryBus.execute(new GetUserSettingsQuery(payload.email));
  }

  @Post()
  async addNewExpense(@Body() newExpenseBody: CreateExpenseDto) {
    const { amount, category, email, name, month } = newExpenseBody;
    try {
      await this.commandBus.execute(
        new CreateExpenseCommand(name, email, amount, category, month),
      );
      return 'Expense Added';
    } catch (err) {
      throw new Error(err.message);
    }
  }

  @Post('settings')
  async upsertUserSettings(@Body() userSettings: UpsertUserSettingsDto) {
    const { email, budget, current_month } = userSettings;
    try {
      await this.commandBus.execute(
        new UpsertUserSettingsCommand(email, budget, current_month),
      );
      return 'Success';
    } catch (err) {
      throw new Error(err.message);
    }
  }
}
