import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserExpense } from '../../entities/user-expense.entity';
import { GetUsersExpenseQuery } from '../get-users-expense.query';

@QueryHandler(GetUsersExpenseQuery)
export class GetUsersExpenseHandler
  implements IQueryHandler<GetUsersExpenseQuery>
{
  constructor(
    @InjectRepository(UserExpense) private repository: Repository<UserExpense>,
  ) {}

  async execute(query: GetUsersExpenseQuery): Promise<any> {
    return await (
      await this.repository.find({ email: query.email })
    ).sort((a, b) => (a.month > b.month ? 1 : -1));
  }
}
