import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserSettings } from '../../entities/user-settings.entity';
import { GetUserSettingsQuery } from '../get-user-settings.query';

@QueryHandler(GetUserSettingsQuery)
export class GetUserSettingsHandler
  implements IQueryHandler<GetUserSettingsQuery>
{
  constructor(
    @InjectRepository(UserSettings)
    private repository: Repository<UserSettings>,
  ) {}

  async execute(query: GetUserSettingsQuery): Promise<any> {
    return await this.repository.findOne({ email: query.email });
  }
}
