const webpack = require('webpack');
require('dotenv').config();

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.currentEnviroment': JSON.stringify('Production'),
      'process.env.BACKEND_URL': JSON.stringify(process.env.BACKEND_URL),
    }),
  ],
};
