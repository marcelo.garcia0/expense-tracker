import { types } from './actions';

export interface UserSettings {
  currentMonth: number;
  budget: number;
}

export interface UserExpense {
  name: string;
  amount: number;
  category: string;
  month: number;
}

export interface UserExpensesState {
  loading: boolean;
  hasErrors: boolean;
  errorMessage: string;
  userExpenses: UserExpense[];
  settings: UserSettings | undefined;
  requestFinished: boolean;
  addExpenseRequestFinished: boolean;
}

const initialState: UserExpensesState = {
  loading: false,
  hasErrors: false,
  errorMessage: '',
  userExpenses: [],
  settings: undefined,
  requestFinished: false,
  addExpenseRequestFinished: false,
};

export default function UserExpenseReducer(state = initialState, action: any) {
  switch (action.type) {
    case types.CREATE_USER_EXPENSE_REQUEST:
      return {
        ...state,
        loading: true,
        hasErrors: false,
        errorMessage: '',
        addExpenseRequestFinished: false,
      };
    case types.CREATE_USER_EXPENSE_SUCCESS:
      return {
        ...state,
        loading: false,
        hasErrors: false,
        errorMessage: '',
        addExpenseRequestFinished: true,
      };
    case types.CREATE_USER_EXPENSE_FAILURE:
      return {
        ...state,
        loading: false,
        hasErrors: true,
        errorMessage: action.payload,
        v: true,
      };

    case types.GET_USER_EXPENSE_REQUEST:
      return {
        ...state,
        loading: true,
        userExpenses: [],
      };
    case types.GET_USER_EXPENSE_SUCCESS:
      return {
        ...state,
        loading: false,
        hasErrors: false,
        errorMessage: '',
        userExpenses: action.payload,
      };
    case types.GET_USER_EXPENSE_FAILURE:
      return {
        ...state,
        loading: false,
        hasErrors: true,
        errorMessage: action.payload,
      };
    case types.GET_USER_SETTINGS_REQUEST:
      return {
        ...state,
        loading: true,
        settings: undefined,
      };
    case types.GET_USER_SETTINGS_SUCCESS:
      return {
        ...state,
        loading: false,
        hasErrors: false,
        errorMessage: '',
        settings: action.payload,
      };
    case types.GET_USER_SETTINGS_FAILURE:
      return {
        ...state,
        loading: false,
        hasErrors: true,
        errorMessage: action.payload,
      };
    case types.RESET_REQUEST_FINISHED:
      return {
        ...state,
        requestFinished: false,
        addExpenseRequestFinished: false,
      };
    default:
      return state;
  }
}

export const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const color = [
  'warning',
  'secondary',
  'success',
  'danger',
  'warning',
  'info',
  'light',
  'info',
  'secondary',
  'success',
  'danger',
  'warning',
];
