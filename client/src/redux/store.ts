import { configureStore } from '@reduxjs/toolkit';
import UserExpenseReducer from './reducer';

export const store = configureStore({
  reducer: { UserExpense: UserExpenseReducer },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
