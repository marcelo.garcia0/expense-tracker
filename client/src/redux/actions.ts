import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { toast } from 'react-toastify';

export const types = {
  CREATE_USER_EXPENSE_REQUEST: 'user_expense/post/pending',
  CREATE_USER_EXPENSE_SUCCESS: 'user_expense/post/fulfilled',
  CREATE_USER_EXPENSE_FAILURE: 'user_expense/post/rejected',
  GET_USER_EXPENSE_REQUEST: 'user_expense/get/pending',
  GET_USER_EXPENSE_SUCCESS: 'user_expense/get/fulfilled',
  GET_USER_EXPENSE_FAILURE: 'user_expense/get/rejected',
  GET_USER_SETTINGS_REQUEST: 'user_settings/get/pending',
  GET_USER_SETTINGS_SUCCESS: 'user_settings/get/fulfilled',
  GET_USER_SETTINGS_FAILURE: 'user_settings/get/rejected',
  UPSERT_USER_SETTINGS_REQUEST: 'user_settings/update/pending',
  UPSERT_USER_SETTINGS_SUCCESS: 'user_settings/update/fulfilled',
  UPSERT_USER_SETTINGS_FAILURE: 'user_settings/update/rejected',
  RESET_REQUEST_FINISHED: 'request_finished/reset',
};

interface CreateExpensePayload {
  name: string;
  email: string;
  amount: number;
  category: string;
  month: number;
}

interface UpsertSettingsPayload {
  email: string;
  budget: number;
  current_month: number;
}

export const createUserExpense = createAsyncThunk(
  'user_expense/post',
  async (payload: CreateExpensePayload, thunkAPI) => {
    try {
      const response = await axios.post(
        `${process.env.BACKEND_URL}/user-expense`,
        payload,
        {},
      );
      if (response) {
        toast.success('Expense added');
      }
      return response.data;
    } catch (error: any) {
      toast.error('An error ocurred');
      return thunkAPI.rejectWithValue(error.response.data.message);
    }
  },
);

export const upsertUserSettings = createAsyncThunk(
  'user_settings/post',
  async (payload: UpsertSettingsPayload, thunkAPI) => {
    try {
      const response = await axios.post(
        `${process.env.BACKEND_URL}/user-expense/settings`,
        payload,
        {},
      );
      if (response) {
        toast.success('Settings Updated');
      }
      return response.data;
    } catch (error: any) {
      toast.error('An error ocurred');
      return thunkAPI.rejectWithValue(error.response.data.message);
    }
  },
);

export const getUserSettings = createAsyncThunk(
  'user_settings/get',
  async (email: string) => {
    const response = await axios.get(
      `${process.env.BACKEND_URL}/user-expense/settings/${email}`,
    );
    return response.data;
  },
);

export const getUserExpenses = createAsyncThunk(
  'user_expense/get',
  async (email: string) => {
    const response = await axios.get(
      `${process.env.BACKEND_URL}/user-expense/${email}`,
    );
    return response.data;
  },
);

export const resetRequestFinished = createAction<void>('requestFinished/reset');
