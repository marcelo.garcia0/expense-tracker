import { useAuth0 } from '@auth0/auth0-react';
import React, { useEffect } from 'react';
import { Routes, Route, NavLink } from 'react-router-dom';
import { AddExpense } from './containers/AddExpense';
import LoginButton from './components/LoginButton';
import LogoutButton from './components/LogoutButton';
import { Overview } from './containers/Overview';
import { Settings } from './containers/Settings';
import { Home } from './containers/Home';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import './styles.scss';

export const App = () => {
  const { isAuthenticated } = useAuth0();

  return (
    <>
      <header className="mb-auto">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">
              Expense Tracker
            </NavLink>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                {isAuthenticated && (
                  <>
                    {' '}
                    <li className="nav-item">
                      <NavLink
                        className="nav-link"
                        aria-current="page"
                        to="/overview"
                        style={({ isActive }) => ({
                          color: isActive ? 'white' : '',
                        })}
                      >
                        Overview
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink
                        className="nav-link"
                        to="/add-expense"
                        style={({ isActive }) => ({
                          color: isActive ? 'white' : '',
                        })}
                      >
                        Add Expense
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink
                        className="nav-link"
                        to="/settings"
                        style={({ isActive }) => ({
                          color: isActive ? 'white' : '',
                        })}
                      >
                        Settings
                      </NavLink>
                    </li>
                  </>
                )}
              </ul>
              <form className="d-flex">
                {isAuthenticated ? <LogoutButton /> : <LoginButton />}
              </form>
            </div>
          </div>
        </nav>
      </header>
      <Routes>
        <Route
          path="/"
          element={<Home userIsAuthenticated={isAuthenticated} />}
        />
        <Route path="/overview" element={<Overview />} />
        <Route path="/settings" element={<Settings />} />
        <Route path="/add-expense" element={<AddExpense />} />
      </Routes>
      <ToastContainer />
    </>
  );
};
