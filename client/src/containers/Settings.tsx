import { useAuth0 } from '@auth0/auth0-react';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { MonthSelect } from '../components/MonthSelect';
import { useDispatch, useSelector } from 'react-redux';
import { getUserSettings, upsertUserSettings } from '../redux/actions';
import { months } from '../redux/reducer';
import { RootState } from '../redux/store';

export const Settings = () => {
  const [currentMonth, setCurrentMonth] = useState('');
  const [budget, setBudget] = useState<number>();

  const { user, isAuthenticated } = useAuth0();
  const { settings } = useSelector((state: RootState) => state.UserExpense);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!isAuthenticated) {
      navigate('/');
    } else {
      if (user && user.email) dispatch(getUserSettings(user.email));
    }
  }, [isAuthenticated]);

  useEffect(() => {
    if (settings) {
      setCurrentMonth(months[settings.current_month]);
      setBudget(settings.budget);
    }
  }, [settings]);

  const shouldBeDisabled = () => currentMonth.length === 0 || !budget || !user;

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (user && budget) {
      dispatch(
        upsertUserSettings({
          budget,
          current_month: months.indexOf(currentMonth),
          email: user.email!,
        }),
      );
    }
  };

  return (
    <div className="container mt-5" style={{ maxWidth: '766px' }}>
      <h1 className="pb-3">Change your settings</h1>
      <form className="container-fuild" onSubmit={handleSubmit}>
        {' '}
        <div className="mb-4">
          <label htmlFor="budgetInput" className="form-label h4 mb-3">
            Budget
          </label>
          <input
            type="number"
            className="form-control"
            id="budgetInput"
            aria-describedby="budgetHelp"
            value={budget}
            placeholder="eg: 15000"
            onChange={(e) => setBudget(parseInt(e.target.value))}
          />
        </div>
        <label htmlFor="amountInput" className="form-label h4 mb-3">
          Current Month{' '}
          <small
            className="text-muted"
            style={{ fontWeight: 400, fontSize: '1rem' }}
          >
            Select your current month
          </small>
        </label>
        <div className="row">
          <div className="col-12 col-md-6">
            <div className="dropdown-menu-dark border-0 pt-2 mx-0 rounded-3 shadow">
              {
                <MonthSelect
                  currentMonth={currentMonth}
                  setCurrentMonth={setCurrentMonth}
                />
              }
            </div>
          </div>
        </div>
        <div className="row d-flex py-5 justify-content-center">
          <div className="col-12 col-md-6">
            <div className="d-grid gap-2">
              <button
                className="btn btn-primary btn-lg"
                type="submit"
                disabled={shouldBeDisabled()}
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};
