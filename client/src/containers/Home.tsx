import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth0 } from '@auth0/auth0-react';

interface Props {
  userIsAuthenticated: boolean;
}

export const Home = ({ userIsAuthenticated }: Props) => {
  const navigate = useNavigate();
  const { loginWithRedirect } = useAuth0();

  const handleOnClick = () => {
    if (userIsAuthenticated) {
      navigate('/overview');
    } else {
      loginWithRedirect();
    }
  };

  return (
    <main>
      <div className="container home-jb px-5 mt-5">
        <div className="p-5 mb-4 bg-light border rounded-3">
          <div className="container-fluid py-5">
            <h1 className="display-5 fw-bold">Expense Tracker</h1>
            <p className="col-md-8 fs-4">
              Expense Tracker helps you track your financial activity
              efficiently. Its simple design makes it lightweight,
              straightforward and very easy to use.
            </p>
            <button
              className="btn btn-primary btn-lg"
              type="button"
              onClick={handleOnClick}
            >
              {userIsAuthenticated
                ? 'Get Started'
                : 'Log in to enable features!'}
            </button>
          </div>
        </div>
      </div>
    </main>
  );
};
