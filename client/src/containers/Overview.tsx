import { useAuth0 } from '@auth0/auth0-react';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ExpenseCard } from '../components/ExpenseCard';
import { getUserExpenses } from '../redux/actions';
import { UserExpense } from '../redux/reducer';
import { RootState } from '../redux/store';

export const Overview = () => {
  const dispatch = useDispatch();

  const { user, isAuthenticated } = useAuth0();
  const { userExpenses, loading } = useSelector(
    (state: RootState) => state.UserExpense,
  );
  const navigate = useNavigate();

  useEffect(() => {
    if (!isAuthenticated) {
      navigate('/');
    } else {
      if (user && user.email) dispatch(getUserExpenses(user.email));
    }
  }, [isAuthenticated]);

  return (
    <>
      <main className="container py-5">
        <h1 className="pb-5">Your expenses overview</h1>
        <div className="row" data-masonry='{"percentPosition": true }'>
          {(userExpenses && userExpenses.length) > 0 ? (
            <>
              {userExpenses.map((expense: UserExpense, i: number) => {
                return <ExpenseCard expense={expense} key={i} />;
              })}
            </>
          ) : (
            <></>
          )}
        </div>
        {loading && (
          <div className="text-center">
            <div className="spinner-border" role="status"></div>
          </div>
        )}
      </main>
    </>
  );
};
