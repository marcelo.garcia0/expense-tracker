import { useAuth0 } from '@auth0/auth0-react';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  createUserExpense,
  getUserSettings,
  resetRequestFinished,
} from '../redux/actions';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../redux/store';

export const AddExpense = () => {
  const [category, setCategory] = useState('');
  const [name, setName] = useState('');
  const [amount, setAmount] = useState<number>();
  const { user, isAuthenticated } = useAuth0();
  const { settings, loading } = useSelector(
    (state: RootState) => state.UserExpense,
  );
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!isAuthenticated) {
      navigate('/');
    } else {
      if (user && user.email) dispatch(getUserSettings(user.email));
    }
  }, [isAuthenticated]);

  const shouldBeDisabled = () =>
    category.length === 0 ||
    name.length === 0 ||
    !amount ||
    !user ||
    !!!settings;

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (
      !!amount &&
      name.length > 0 &&
      category.length > 0 &&
      user &&
      settings
    ) {
      dispatch(
        createUserExpense({
          amount,
          name,
          category,
          month: settings.current_month,
          email: user.email!,
        }),
      );

      setName('');
      setAmount(0);
      setCategory('');
    }
  };

  return (
    <div className="container mt-5" style={{ maxWidth: '766px' }}>
      <h1 className="pb-3">Add a new expense</h1>
      <form className="container-fuild" onSubmit={handleSubmit}>
        {' '}
        <div className="mb-4">
          <label htmlFor="nameInput" className="form-label h4 mb-3">
            Name
          </label>
          <input
            type="text"
            className="form-control"
            id="nameInput"
            aria-describedby="nameHelp"
            placeholder="Enter the name of the expense"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </div>
        <div className="mb-4">
          <label htmlFor="amountInput" className="form-label h4 mb-3">
            Amount
          </label>
          <input
            type="number"
            className="form-control"
            id="amountInput"
            aria-describedby="amountHelp"
            placeholder="eg: 15000"
            value={amount}
            onChange={(e) => setAmount(parseInt(e.target.value))}
          />
        </div>
        <label htmlFor="amountInput" className="form-label h4 mb-3">
          Category{' '}
          <small
            className="text-muted"
            style={{ fontWeight: 400, fontSize: '1rem' }}
          >
            Select the type of expense
          </small>
        </label>
        <div className="row">
          <div className="col-12 col-md-6">
            <div className="dropdown-menu-dark border-0 pt-2 mx-0 rounded-3 shadow">
              <ul className="list-unstyled mb-0">
                <li>
                  <span
                    className={`dropdown-item ${
                      category === 'Wages' ? 'dropdown-item-active' : ''
                    } d-flex align-items-center gap-2 py-2`}
                    onClick={() => setCategory('Wages')}
                    role="button"
                  >
                    <span
                      className="d-inline-block bg-success rounded-circle"
                      style={{ width: '.5em', height: '.5em' }}
                    ></span>
                    Wages
                  </span>
                </li>
                <li>
                  <span
                    className={`dropdown-item ${
                      category === 'Rent' ? 'dropdown-item-active' : ''
                    } d-flex align-items-center gap-2 py-2`}
                    onClick={() => setCategory('Rent')}
                    role="button"
                  >
                    <span
                      className="d-inline-block bg-info rounded-circle"
                      style={{ width: '.5em', height: '.5em' }}
                    ></span>
                    Rent
                  </span>
                </li>
                <li>
                  <span
                    className={`dropdown-item ${
                      category === 'Clothing' ? 'dropdown-item-active' : ''
                    } d-flex align-items-center gap-2 py-2`}
                    onClick={() => setCategory('Clothing')}
                    role="button"
                  >
                    <span
                      className="d-inline-block bg-danger rounded-circle"
                      style={{ width: '.5em', height: '.5em' }}
                    ></span>
                    Clothing
                  </span>
                </li>
                <li>
                  <span
                    className={`dropdown-item ${
                      category === 'Food' ? 'dropdown-item-active' : ''
                    } d-flex align-items-center gap-2 py-2`}
                    onClick={() => setCategory('Food')}
                    role="button"
                  >
                    <span
                      className="d-inline-block bg-warning rounded-circle"
                      style={{ width: '.5em', height: '.5em' }}
                    ></span>
                    Food
                  </span>
                </li>
                <li>
                  <span
                    className={`dropdown-item ${
                      category === 'Others' ? 'dropdown-item-active' : ''
                    } d-flex align-items-center gap-2 py-2`}
                    onClick={() => setCategory('Others')}
                    role="button"
                  >
                    <span
                      className="d-inline-block bg-light rounded-circle"
                      style={{ width: '.5em', height: '.5em' }}
                    ></span>
                    Others
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="row d-flex py-5 justify-content-center">
          <div className="col-12 col-md-6">
            <div className="d-grid gap-2">
              <button
                className="btn btn-primary btn-lg"
                type="submit"
                disabled={shouldBeDisabled() || loading}
              >
                {loading && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                <span style={{ marginLeft: '1rem' }}>Submit</span>
              </button>
              {!!!settings && !loading && (
                <p className="card-text">
                  <small style={{ color: 'red' }}>
                    Please update your settings to add an expense.
                  </small>
                </p>
              )}
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};
