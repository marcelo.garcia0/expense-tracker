import React from 'react';
import { color, months, UserExpense } from '../redux/reducer';

interface Props {
  expense: UserExpense;
}

export const ExpenseCard = ({ expense }: Props) => {
  return (
    <div className="col-sm-6 col-lg-4 mb-4">
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{expense.name}</h5>
          <div className="card-text">
            <ul style={{ listStyleType: 'none', paddingLeft: '0rem' }}>
              <li>
                <span
                  className={`d-inline-block bg-${
                    color[Math.floor(Math.random() * 12)]
                  } rounded-circle`}
                  style={{
                    width: '.5em',
                    height: '.5em',
                    marginRight: '0.5rem',
                  }}
                ></span>
                Category: {expense.category}
              </li>
              <li>
                {' '}
                <span
                  className={`d-inline-block bg-${
                    color[Math.floor(Math.random() * 12)]
                  } rounded-circle`}
                  style={{
                    width: '.5em',
                    height: '.5em',
                    marginRight: '0.5rem',
                  }}
                ></span>
                Amount: ${expense.amount}
              </li>
            </ul>
          </div>
          <p className="card-text">
            <small className="text-muted">{months[expense.month]}</small>
          </p>
        </div>
      </div>
    </div>
  );
};
