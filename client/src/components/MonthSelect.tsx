import React from 'react';
import { color, months } from '../redux/reducer';

interface Props {
  setCurrentMonth: (month: string) => void;
  currentMonth: string;
}

export const MonthSelect = ({ setCurrentMonth, currentMonth }: Props) => {
  return (
    <ul className="list-unstyled mb-0">
      {months.map((month, i) => {
        return (
          <li key={month}>
            <span
              className={`dropdown-item ${
                currentMonth === month ? 'dropdown-item-active' : ''
              } d-flex align-items-center gap-2 py-2`}
              onClick={() => setCurrentMonth(month)}
              role="button"
            >
              <span
                className={`d-inline-block bg-${color[i]} rounded-circle`}
                style={{ width: '.5em', height: '.5em' }}
              ></span>
              {month}
            </span>
          </li>
        );
      })}
    </ul>
  );
};
